# Documentation repositorty for 5 Countries

This is the documentation of group 3 for the 5 countries project at EAL fall 2017.

## Folders

* `Assorted images`: Some assorted images for the project.
* `Business Case`: Files for the business case.
* `HLD`: Files for the internal high level design.
* `LLD`: Files for the internal low level design.
* `Lessons Learned`: Various documenbts with lessons learned during the project.
* `MPLS`: Files for MPLS backbone ducumentation.
* `Network diagrams`: Files for network diagrams.
* `Project plan`: Files forthe project plan.
* `Stage-1 End Report`: Files for Stage 1 end report.
* `Stage-2 End report`: Files for Stage 2 end report.
* `Stage-3 End Report`: Files for Stage 3 end report.
* `Weekly presentations`: Slides from weekly presentations.
* `meetings`: Notes from meetings.
* `report`: Files for the final report.
* `Report structure.md`: Early notes on final report structure.
