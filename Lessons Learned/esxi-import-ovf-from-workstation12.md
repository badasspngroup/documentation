# VMWare Workstation 12 Machine import on ESXi 6.0

#New VM

Running VMWare Workstation 12 on the local machine and building virtual machines
that can be imported in ESXi 6.0 is quite possible. Make sure when you create
the VM in Workstation that you select Hardware Compatibility to Workstation 11.

![VMware Workstation -Hardware Compatibility dialog](work12_hc.png "VMware Workstation -Hardware Compatibility dialog")

# Existing VM

You can change this setting on an existing VM by right clicking, selecting "Manage" -> "Change Hardware Compatibility"

![VMware Workstation -Hardware Compatibility dialog](work_manage.png "VMware Workstation -Hardware Compatibility dialog")

# VM imported in ESXi

ESXi might not be up to speed with the operating system type, so check that in
the imported VM settings.
