# VMWare knowledge base

There is a note about a situation that seems to touch on the same subject in VMWare Knowlegde Base article [103111](https://kb.vmware.com/selfservice/microsites/search.do?language=en_US&cmd=displayKC&externalId=1031111).

"Under certain circumstances there may be a situation where a duplicate mac address arises on the network due to ESXi vmkernel maintaining the mac address of a previous ESXi installation on a different physical server. "

# MAC addresses after installation.

## Host NIC configuration

![VMware host NIC MAC 00:0c:29:d8:3d:2c](host_nic.png "MAC 00:0c:29:d8:3d:2c")

## Wireshark

![Wireshark of the DHCP request](wireshark.png "Wireshark showing MAC 00:0c:29:d8:3d:2c")  

# OK?

So on installation ESXi kernel NIC picks up the MAC address of the "physical" (VMWare host) interface as expected.

# Changing the MAC of the host

![VMware host NIC MAC 00:50:56:37:C8:A4](host_nic_new.png "MAC 00:50:56:37:C8:A4")

...and reboot ESXi...

## Wireshark

![Wireshark of the DHCP request](wireshark_new.png "Wireshark showing MAC 00:0c:29:d8:3d:2c")

Still using the old MAC address for the request...

# Following the advice in the KB article

The KB article advices that removing the kernel NIC from the Port Group, and re-adding it will change the MAC.

## ESXi shell kernle NIC reset

![Reset kernel NIC interface using ESXi shell](reset.png "Resetting the Kernel NIC")

(I am listing the wrong interfaces here, command should have been `esxcfg-vmknic -l`)

## Wireshark

![Wireshark of the DHCP request](wireshark_reset.png "Wireshark showing MAC 00:50:56:37:C8:A4")

New MAC!

# Conclusions

 * It is not logical that the vSwitch should have a MAC (?)
 * The kernel NIC, at least in some situations, keeps the old MAC address after
   it has changed.

If an installation is moved to another node the old MAC address might stick. If
a clean installation of the ESXi was done on the old node, that installation would receive the same IP address from the DHCP server, because of the now duplicate MAC addresses.

# Remedy

The only solutions that worked on our installation of ESXi 6.0 was one of two:

 * Wiping the installation hard drive using Debian, and reinstalling vSphere 6.0
 * Resetting the Management Network stack in the ESXi control interface on the
 physical console.

These methods were also tested:

* Use `esxcfg-advcfg -s 1 /Net/FollowHardwareMac` in the ESXi shell, the workaround recommended by [103111](https://kb.vmware.com/selfservice/microsites/search.do?language=en_US&cmd=displayKC&externalId=1031111).
* Enable the shell and reset the kernel NIC using the steps advised in [103111](https://kb.vmware.com/selfservice/microsites/search.do?language=en_US&cmd=displayKC&externalId=1031111).
