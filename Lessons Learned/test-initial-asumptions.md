# Setup

We were given access to ESXi running on a node in the server room. The nodes ran in blades 2 blades of 4 nodes. Other groups were having problems with their ESXi installations, we had not really used ours, only confirmed that we could login.

# Initial problem

At some point people outside our group started looking into the storage configuration and concluded these things:

 * Not all drives were detected.
 * Drives were different sizes among nodes.

Hard disk were swapped between nodes in an effort to fix the problems and make
hardware configurations among the nodes equal.

This created a new problem as ESXi installation were switched between nodes. It turns out that ESXi will not pick up the new MAC address of the physical interface, but keep the old one on the internal VMK0 interface. This manifested itself in a situation where multiple nodes had the same MAC address on the
interfaces in the management network.

# Solutions

 * Ask for documentation on pre installed systems and hardware early in the project.
 * Test promises made in the provided documentation early in the project.

# Lessons

Besides the solutions mentioned above, this was also a matter of communication, since none of the problems where clearly communicated to all involved partners. Some involved were at other stages in the process, not really ready to assign attention to the problem.

Communication platform might be decided among all involved partners at the beginning of the project in any manner that works for the involved, democratic, dictatorial...
