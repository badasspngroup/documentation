1. Intro
  * Scope
  * Requirements
  * Use case
2. Business Case
3. Project management
  * Project Plan
  * Asana
  * MS Project (**Managed by stages** should be written somewhere in this chapter)
  * Risk management
4. HLD
5. LLD
  * Technical
  * Link to config files on GitHub
  * Link to deployment scripts on GitHub
  * Issues
6. Conclusion
