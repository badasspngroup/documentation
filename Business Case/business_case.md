# Executive Summary

The aim in this project is to develop a network infrastructure, to connect a data center in Denmark to the internet and the rest of the world.

In each big city (Copenhagen, Odense, Aalborg) we’ll have a router to make an internal network – besides that, Copenhagen will be connected to a MPLS backbone network, with the rest of the world.

In each environment, we use Juniper products like vMX and vSRX – everything in the environment will be built on top of a hyper-visor. The data center will also have a few servers at each location.

There will be a VPN tunnel to be connected to, running on an application level - followed by a jump host.

# Reasons

We have a Danish customer who wants to be connected to the internet, but still have an internal network within Denmark – He has a data center in Aalborg, Odense, Copenhagen.

The customer is a data center, which means firewalls, routing and high availability is essential.

For the data center to get customers it needs:

 * Internet connection
 * Security
 * Connection to MPLS backbone network
 * Servers
 * VPN connection

Additionally we are going to provide these services:

 * Internally running IPv6
 * Juniper devices for routing and switching.
 * Monitoring and redundancy for securing high availability

It’s important for the customer, we keep it at low cost long term, but necessary price short term.

# Business Options

The recommended business option is to place a vMX in each site(Aalborg, Odense and Copenhagen) and an vSRX as a firewall between Copenhagen and the SRX(Running MPLS)
The vSRX and vMX are running on top of a type 1 hyper-visor.

In the network, we’ll have:
 * Log server/Elastic Stack server
 * DHCP server
 * DNS server
 * Web server
 * File server

We need theses servers in the network, to make it fully functional.
More can be found in the HLD, for this project.

# Expected Benefits

Expected benefits of this project are to create a secure connection between the Danish cities, and servers and MPLS backbone.


# Expected Dis-benefits
*(Outcomes perceived as negative by one or more stakeholders. Dis-benefits are actual consequences of an activity whereas, by definition, a risk has some uncertainty about whether it will materialize. For example, a decision to merge two elements of an organization onto a new site may have benefits (e.g. better joint working), costs (e.g. expanding one of the two sites) and dis-benefits (e.g. drop in productivity during the
merger). Dis-benefits need to be valued and incorporated into the investment appraisal)*

# Timescale

We are going to follow project plan, made by PM in Microsoft Project.

# Costs

| Description          | Cost              |
|:---------------------|:-----------------:|
| Cost of work         |     1.000.000 DKK |
| Cost of hardware     |       176.000 DKK |
| Risk (uncertainties) |       440.500 DKK |
| **Total**            | **2.378.500 DKK** |

# Investment Appraisal
This project will provide secure and fast internet connection between the cities in Denmark and the rest of Europe. According to our risk management strategy we know that, we can control almost every risk during this project. The biggest benefit for us is improvement of knowledge in networking, security and management areas.

# Major Risks

 * Failing to acquire needed knowledge
 * Hardware problems
 * Resource depletion
 * Hacking
 * Hardware having wrong initial configuration
 * Cyber warfare

# Risk Management Strategy

During this project we are going face threads and opportunities. Our goal is to identify, and control them.
Risk procedure:
Identify -> Assess -> Plan -> Implement

After every uncertainty we are going to make document about lessons learned. Including flowchart diagram of SWOT.

According to the list of major risks we decided to assign solution to each risk.

 * Failing to acquire needed knowledge
   * Involve external consultants
 * Hardware problems
   * Replace hardware
   * Scale down requirements if possible
 * Resource depletion
 * Hacking
 * Testing
   * Review of configurations by at least one other member of the team.
 * Hardware having wrong initial configuration
   * Ask the supplier to fix these errors
   * Fix the errors ourselves

# Conclusion

In developing the data center described in this Business Case will bring benefits in these areas:

 * Security
 * Future proof protocols
 * Network segregated by VLANS
 * Monitoring and redundancy for high availability

The immediate investment is 2.378.500 DKK, after which the cost will be mainly
the resources needed for running the data center (Electricity, connection, etc.).

The team has a knowledge in networking, virtualisation, security and infrastructure. As Juniper Certified Associates the team possesses the most up to date networking knowledge in the field. We are highly qualified for this Project.

Going forward with this project will:

 * Update, secure and optimise the infrastructure.
 * Automate simple maintenance tasks
 * Provide analysis of current health
 * Provide notification, should any problem arise
